module.exports = {
  content: ['./src/**/*.{html,js,jsx}'],
  theme: {
    extend: {
      colors: {
        'case-study-gray': {
          100: '#95969A',
          200: '#5E6060',
          300: '#232525',
        },
        'case-study-yellow': {
          100: 'rgba(255, 205, 0, 0.08)',
          200: '#FFCD00',
        },
      },
      fontFamily: {
        roboto: ['Roboto', 'sans-serif'],
        DM: ['DM Sans', 'normal'],
      },
    },
  },
  plugins: [],
};
