import React from "react";
import Cover from "../components/Cover";
import FormContainer from "../components/Form/FormContainer";

const LandingPage = () => {
  return (
    <div className="flex flex-col-reverse overflow-hidden lg:flex-row lg:h-screen">
      <div className="w-full flex lg:h-auto lg:w-2/4">
        <FormContainer />
      </div>
      <div className="w-full flex-1 overflow-hidden lg:h-auto lg:w-2/4 max-h-[50vh] lg:max-h-[100%] lg:max-h-auto">
        <Cover />
      </div>
    </div>
  );
};

export default LandingPage;
