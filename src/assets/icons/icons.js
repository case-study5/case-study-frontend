const Arrow = ({ size, className = null, color = '#FFF' }) => (
  <svg
    width={size}
    height={size}
    className={className}
    viewBox='0 0 17 16'
    fill={color}
  >
    <path
      d='M8.5 0L7.09 1.41L12.67 7H0.5V9H12.67L7.09 14.59L8.5 16L16.5 8L8.5 0Z'
      fill={color}
    />
  </svg>
);

export { Arrow };
