const rooms = [
  { value: 1, label: "1-10" },
  { value: 2, label: "10-50" },
  { value: 3, label: "50-100" },
  { value: 4, label: "100-200" },
  { value: 5, label: "200+" },
];

const hotel = [
  { value: true, label: "Oui" },
  { value: false, label: "Non" },
];

const sits = [
  { value: 1, label: "1-10" },
  { value: 2, label: "10-25" },
  { value: 3, label: "25-50" },
  { value: 4, label: "50+" },
];

export { rooms, hotel, sits };
