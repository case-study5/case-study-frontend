import React from "react";
import { Arrow } from "../../assets/icons/icons";

const NextStepButton = ({ className, isValid, ...props }) => {
  return (
    <button
      className={`font-DM font-bold tracking-wider flex items-center justify-center 
      rounded-md w-full p-2 py-3 ${className}
      ${
        isValid
          ? "bg-case-study-gray-300 text-white"
          : "bg-neutral-200 text-zinc-400"
      }`}
      {...props}
      disabled={!isValid}
    >
      Étape suivante
      <Arrow size={18} className="mx-2" color={"currentColor"} />
    </button>
  );
};

export default NextStepButton;
