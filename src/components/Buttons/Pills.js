import React from "react";

const Pills = ({ children, className, isActive, ...props }) => {
  return (
    <span
      className={`cursor-pointer border p-3 pb-2 px-6 rounded-lg select-none font-DM whitespace-nowrap
    ${
      isActive
        ? "border-case-study-yellow-200 bg-case-study-yellow-100 text-case-study-yellow-200"
        : "border-case-study-gray-100 text-case-study-gray-100"
    } ${className}`}
      {...props}
    >
      {children}
    </span>
  );
};

export default Pills;
