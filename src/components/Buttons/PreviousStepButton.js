import React from 'react';
import { Arrow } from '../../assets/icons/icons';

const PreviousStepButton = ({
  className,
  isValid,
  setReset,
  reset,
  ...props
}) => (
  <button
    className={`font-DM font-bold tracking-wider text-sm flex items-center justify-center 
    rounded-md w-full p-2 py-3 bg-transparent text-gray-400 hover:text-gray-500 ${className}`}
    onClick={() => setReset(!reset)}
    {...props}
  >
    <Arrow size={12} className='mx-2 rotate-180' color={'currentColor'} />{' '}
    Retour
  </button>
);

export default PreviousStepButton;
