import axios from "axios";
import React, { useEffect, useState } from "react";
import NextStepButton from "../Buttons/NextStepButton";
import FormComponent from "./FormComponent";
import { rooms, sits, hotel } from "../../utils/data";

const Form = ({ setIsValid, isValid, reset }) => {
  const [roomState, setRoomState] = useState();
  const [hotelState, setHotelState] = useState();
  const [sitsState, setSitsState] = useState();

  useEffect(() => {
    const isValid = roomState?.value && hotelState?.value && sitsState?.value;
    if (isValid) setIsValid(isValid);
    if (!hotelState?.value) setIsValid(false);
  }, [roomState, hotelState, sitsState]);

  useEffect(() => {
    setRoomState(" ");
    setHotelState(" ");
    setSitsState(" ");
  }, [reset]);

  const submitHandler = (e) => {
    e.preventDefault();
    const data = {
      rooms: roomState,
      hotel: hotelState,
      sits: sitsState,
    };
    axios
      .post("http://localhost:5000/api", data)
      .then((res) => console.log("response =>", res))
      .catch((err) => console.error("error =>", err));
    setRoomState();
    setHotelState();
    setSitsState();
  };

  return (
    <form className="mt-14 mb-4" onSubmit={submitHandler}>
      <FormComponent
        message={"Combien de chambre avez-vous dans votre hÃ´tel ?"}
        data={rooms}
        setState={setRoomState}
      />
      {roomState?.value && (
        <FormComponent
          message={"Avez-vous un restaurant ?"}
          data={hotel}
          setState={setHotelState}
        />
      )}
      {hotelState?.value && (
        <FormComponent
          message={"Combien de places assises avez-vous au restaurant ?"}
          data={sits}
          setState={setSitsState}
        />
      )}

      <NextStepButton type="submit" isValid={isValid} className="mt-4" />
    </form>
  );
};

export default Form;
