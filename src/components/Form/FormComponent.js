import React, { useEffect, useState } from "react";
import Pills from "../Buttons/Pills";

const FormComponent = ({ message, data, setState }) => {
  const [isActive, setIsActive] = useState({});

  const onPillClickHandler = (item) => {
    setIsActive(item);
    setState(item);
  };

  return (
    <div className="mt-4 text-case-study-gray-200 font-bold ">
      {message}
      <div className="flex flex-row space-x-3 p-1 py-3 overflow-x-scroll lg:overflow-x-auto">
        {data?.map((item, i) => {
          return (
            <Pills
              key={i}
              isActive={isActive.value === item.value}
              onClick={() => onPillClickHandler(item)}
            >
              {item.label}
            </Pills>
          );
        })}
      </div>
    </div>
  );
};

export default FormComponent;
