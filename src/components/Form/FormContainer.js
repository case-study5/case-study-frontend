import React, { useState } from "react";
import PreviousStepButton from "../Buttons/PreviousStepButton";
import Header from "../Header";
import Form from "./Form";

const FormContainer = () => {
  const [isValid, setIsValid] = useState(false);
  const [reset, setReset] = useState(false);

  return (
    <div className="flex flex-col  justify-center px-6 lg:px-10 w-full">
      <Header title={"Votre établissement"} />
      <div className="text-left text-case-study-gray-300  font-DM tracking-wider w-full lg:w-10/12">
        Ces informations importantes serviront à mieux connaître votre
        établissement et à adapter notre communication en fonction de vos
        réponses.
      </div>
      <div>
        <Form setIsValid={setIsValid} isValid={isValid} reset={reset} />
      </div>
      <PreviousStepButton setReset={setReset} reset={reset} className="mt-4" />
    </div>
  );
};

export default FormContainer;
