import React from 'react';

const Header = ({ title, className }) => {
  return (
    <div
      className={`text-4xl text-case-study-gray-300 py-6 font-bold ${className}`}
    >
      {title}
    </div>
  );
};

export default Header;
