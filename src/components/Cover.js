import React from "react";
import Picture from "../assets/images/picture.jpeg";

const Cover = () => (
  <img
    className=" w-screen lg:w-auto lg:h-screen"
    src={Picture}
    alt={"Picture"}
  />
);

export default Cover;
